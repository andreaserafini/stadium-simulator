# Stadium simulator
An Android app developed using OpenGLES that simulates a new generation stadium. The stadium is capable of switching between different courts (for example a football court and a basketball court).  
The aim of the simulation is to show the user the details of the mounting/unmounting process.

## Mobile Programming exam
This project was implemented to take the Mobile Programming exam at the University of Modena and Reggio Emilia (Computer Science master's degree, 2022).

## Implementation details
Implementation details will be described in the project's wiki page.
