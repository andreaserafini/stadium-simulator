package com.example.stadiumsimulator;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;

import java.util.Timer;

public class MainActivity extends Activity
        implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {

    private GLSurfaceView surface;
    private GLSurfaceView.Renderer renderer;
    private GestureDetector gestureDetector;
    private boolean isSurfaceCreated;

    private FootballCourt footballCourt;
    private BasketballCourt basketballCourt;
    private MatrixUpdater matrixUpdater;
    private Timer timer;
    private Stadium stadium;
    private StadiumRenderer stadiumRenderer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* UNCOMMENT THE FOLLOWING LINES TO SEE A MORE DETAILED LOG */
        /*StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectLeakedClosableObjects()
                .penaltyLog()
                .build());*/

        int supp;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final ActivityManager activityManager =
                (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        final ConfigurationInfo confInfo = activityManager.getDeviceConfigurationInfo();
        if(confInfo.reqGlEsVersion >= 0x30000){
            supp = 3;
        }else if(confInfo.reqGlEsVersion >= 0x20000){
            supp = 2;
        }else{
            supp = 1;
        }

        Log.v("TAG","Opengl ES supported >= " +
                supp + " (" + Integer.toHexString(confInfo.reqGlEsVersion) + " " +
                confInfo.getGlEsVersion() + ")");

        surface = new GLSurfaceView(this);
        surface.setEGLContextClientVersion(supp);
        surface.setPreserveEGLContextOnPause(true);

        this.footballCourt = new FootballCourt();
        this.basketballCourt = new BasketballCourt();
        this.stadium = new Stadium( this); // Context needed
        this.stadiumRenderer = new StadiumRenderer(this.stadium,
                this.basketballCourt,
                this.footballCourt);

        this.renderer = stadiumRenderer;

        timer = new Timer();
        this.matrixUpdater = new MatrixUpdater(stadiumRenderer, this.stadium,
                this.footballCourt, this.basketballCourt);
        timer.scheduleAtFixedRate(this.matrixUpdater,0, 10);


        setContentView(surface);
        ((BasicRenderer) renderer).setContext(this);

        surface.setRenderer(renderer);

        gestureDetector = new GestureDetector(this, this);
        gestureDetector.setOnDoubleTapListener(this);

        isSurfaceCreated = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        this.gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @Override
    public void onResume(){
        super.onResume();
        if(isSurfaceCreated){
            surface.onResume();
        }
        matrixUpdater.wakeUp();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        matrixUpdater.cancel();
        timer.purge();
        timer.cancel();
    }

    @Override
    public void onStart(){
        super.onStart();
        matrixUpdater.wakeUp();
    }

    @Override
    public void onPause(){
        super.onPause();
        if(isSurfaceCreated)
            surface.onPause();
        matrixUpdater.sleep();
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        ((StadiumRenderer) this.renderer).doubleTapHandler();
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        return true;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) { }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }
}
