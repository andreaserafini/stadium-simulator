package com.example.stadiumsimulator;

import android.opengl.GLES30;

import java.util.concurrent.atomic.AtomicInteger;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static android.opengl.GLES10.GL_CCW;
import static android.opengl.GLES20.GL_BACK;
import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_CULL_FACE;
import static android.opengl.GLES20.GL_DEPTH_BUFFER_BIT;
import static android.opengl.GLES20.GL_DEPTH_TEST;
import static android.opengl.GLES20.GL_LEQUAL;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glCullFace;
import static android.opengl.GLES20.glDepthFunc;
import static android.opengl.GLES20.glDrawElements;
import static android.opengl.GLES20.glEnable;
import static android.opengl.GLES20.glFrontFace;
import static android.opengl.GLES20.glGenBuffers;
import static android.opengl.GLES20.glGenTextures;
import static android.opengl.GLES20.glUniform3fv;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glVertexAttribPointer;

public class StadiumRenderer extends BasicRenderer {

    /** all possible simulation states here */
    public enum STATE {
        COURT_VIS,
        CUTTING_STADIUM,
        RESTORING_STADIUM,
        COURT0_OUT,
        COURT1_IN,
        COURT1_OUT,
        COURT0_IN
    }

    /** all courts here */
    public enum COURT {
        FOOTBALL,
        BASKET
    }

    private STATE curr_state = STATE.COURT_VIS; /** current simulation state */
    private COURT curr_court = COURT.FOOTBALL;  /** current visible court */


    private int[] VAO; /** a single VAO object */

    private final float[] lightPos;
    private final float[] eyePos;

    private AtomicInteger cutoff;
    private AtomicInteger angle;

    private FootballCourt footballCourt;
    private BasketballCourt basketballCourt;
    private Stadium stadium;

    private static final int footballTextureIndex = 0;
    private static final int basketballTextureIndex = 1;

    /** starting coutoff level, with this cutoff level the stadium is completely visible */
    public static final int     CUTOFF_STARTING_VALUE = -90;

    /** court disposal can be either synchronized (true) or not (false) */
    public static final boolean SYNCHRONIZE_DISPOSAL  = false;

    public StadiumRenderer(Stadium stadium, BasketballCourt basketballCourt, FootballCourt footballCourt) {
        super();
        lightPos = new float[]{1f, 1f, 5f};
        eyePos = new float[]{0f, 1f, 2f};

        this.stadium = stadium;
        this.footballCourt = footballCourt;
        this.basketballCourt = basketballCourt;

        this.angle = new AtomicInteger(0);
        this.cutoff = new AtomicInteger(CUTOFF_STARTING_VALUE);
    }

    public void doubleTapHandler() {
        if(this.curr_state == STATE.COURT_VIS)
            this.curr_state = STATE.CUTTING_STADIUM;
    }

    /* ------------------- */
    /* - GETTERS/SETTERS - */
    /* ------------------- */

    public float[] getLightPos() {
        return lightPos;
    }

    public float[] getEyePos() {
        return eyePos;
    }

    public AtomicInteger getCutoff() {
        return cutoff;
    }

    public AtomicInteger getAngle(){
        return this.angle;
    }

    public STATE getCurrentState() {
        return this.curr_state;
    }

    public COURT getCurrCourt() {
        return this.curr_court;
    }

    public void setCurrCourt(COURT curr_court) {
        this.curr_court = curr_court;
    }

    public void setCurrentState(STATE state) {
        this.curr_state = state;
    }

    /* -----------------------*/
    /* - GETTERS/SETTERS END -*/
    /* -----------------------*/

    @Override
    public void onSurfaceChanged(GL10 gl10, int w, int h) {
        super.onSurfaceChanged(gl10, w, h);

        float aspect = ((float) w) / ((float) (h == 0 ? 1 : h));

        stadium.setupViewMatrices(aspect, this.eyePos);
        footballCourt.setupViewMatrices(aspect, this.eyePos);
        basketballCourt.setupViewMatrices(aspect, this.eyePos);
    }

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {
        super.onSurfaceCreated(gl10, eglConfig);

        VAO = new int[3]; /*- ONE FOR THE STADIUM, ONE FOR THE FOOTBALL COURT, ONE FOR BASKET -*/
        GLES30.glGenVertexArrays(3, VAO, 0);

        stadium.setupBuffers(VAO, 0);
        stadium.compileShader(this);

        footballCourt.setupBuffers(VAO, 1);
        footballCourt.compileShader();
        footballCourt.loadTexture(this.getContext(), footballTextureIndex, R.drawable.scfield);

        basketballCourt.setupBuffers(VAO, 2);
        basketballCourt.compileShader();
        basketballCourt.loadTexture(this.getContext(), basketballTextureIndex, R.drawable.bbfield);
        basketballCourt.setDisposed();

        /*- ENABLE DEPTH TEST -*/
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        /*- ENABLE FACE CULLING -*/
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glFrontFace(GL_CCW);
    }

    @Override
    public void onDrawFrame(GL10 gl10) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        switch(this.getCurrentState()){

            case COURT_VIS:
                this.getAngle().incrementAndGet();
                break;

            case CUTTING_STADIUM:

                this.getAngle().incrementAndGet();

                /* STOPPING AT A KNOWN ANGLE */
                if(this.getCurrCourt() == StadiumRenderer.COURT.FOOTBALL)
                    this.setCurrentState(StadiumRenderer.STATE.COURT0_OUT);
                else
                    this.setCurrentState(StadiumRenderer.STATE.COURT1_OUT);
                break;

            case RESTORING_STADIUM:

                this.getAngle().incrementAndGet();

                /* RESTORING PART OF STADIUM */
                if(this.getCutoff().get() > CUTOFF_STARTING_VALUE){
                    this.getCutoff().decrementAndGet();
                }else{
                    if(this.getCurrCourt() == StadiumRenderer.COURT.FOOTBALL){
                        this.setCurrCourt(StadiumRenderer.COURT.BASKET);
                    }else{
                        this.setCurrCourt(StadiumRenderer.COURT.FOOTBALL);
                    }
                    this.setCurrentState(StadiumRenderer.STATE.COURT_VIS);
                }
                break;

            case COURT0_OUT:
                if(SYNCHRONIZE_DISPOSAL){
                    courtOutSynch(this.footballCourt, STATE.COURT1_IN);
                }else{
                    courtOutNotSynch(this.footballCourt, STATE.COURT1_IN);
                }
                break;

            case COURT1_IN:
                if(!basketballCourt.resumeCourt())
                    this.setCurrentState(StadiumRenderer.STATE.RESTORING_STADIUM);
                break;

            case COURT0_IN:
                if(!footballCourt.resumeCourt())
                    this.setCurrentState(StadiumRenderer.STATE.RESTORING_STADIUM);
                break;

            case COURT1_OUT:

                if(SYNCHRONIZE_DISPOSAL){
                    courtOutSynch(this.basketballCourt, STATE.COURT0_IN);
                }else{
                    courtOutNotSynch(this.basketballCourt, STATE.COURT0_IN);
                }

                break;
        }

        // Restoring angle:
        int angle = this.getAngle().get();
        if (angle >= 360) {
            angle -= 360;
            this.getAngle().set(angle);
        }

        /* draw calls */
        stadium.drawStadium(VAO, 0);
        footballCourt.drawCourt(VAO,1, footballTextureIndex);
        basketballCourt.drawCourt(VAO,2, basketballTextureIndex);
    }

    private void courtOutNotSynch(Court oldCourt, STATE newState){
        if(!(getAngle().get() >= 200 && getAngle().get() <= 203)){
            /* STOPPING THE STADIUM AT A KNOWN ANGLE */
            this.getAngle().incrementAndGet();
            this.getAngle().incrementAndGet();
        }
        /* REMOVING PART OF THE STADIUM WHILE TURNING */
        if(this.getCutoff().get() < 0)
            this.getCutoff().incrementAndGet();

        /* DISPOSE COURT WHILE CUTTING STADIUM */
        if(!oldCourt.disposeCourt())
            this.setCurrentState(newState);
    }

    private void courtOutSynch(Court oldCourt, STATE newState){
        if(!(getAngle().get() > 200 && getAngle().get() < 203)){
            /* STOPPING THE STADIUM AT A KNOWN ANGLE */
            this.getAngle().incrementAndGet();
            this.getAngle().incrementAndGet();
        }else{
            /* REMOVING PART OF THE STADIUM ONLY AFTER BEING STOPPED*/
            if(this.getCutoff().get() < 0)
                this.getCutoff().incrementAndGet();

            /* DISPOSE COURT WHILE CUTTING STADIUM */
            if(!oldCourt.disposeCourt())
                this.setCurrentState(newState);
        }

    }

}
