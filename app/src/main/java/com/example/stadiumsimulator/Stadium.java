package com.example.stadiumsimulator;

import static android.opengl.GLES20.GL_ARRAY_BUFFER;
import static android.opengl.GLES20.GL_ELEMENT_ARRAY_BUFFER;
import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_STATIC_DRAW;
import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.GL_UNSIGNED_INT;
import static android.opengl.GLES20.glBindBuffer;
import static android.opengl.GLES20.glBufferData;
import static android.opengl.GLES20.glDrawElements;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGenBuffers;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniform1f;
import static android.opengl.GLES20.glUniform3fv;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;

import android.content.Context;
import android.opengl.GLES30;
import android.opengl.Matrix;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.concurrent.atomic.AtomicInteger;

public class Stadium {

    private AtomicInteger rotationAngle;
    private AtomicInteger cutoffLevel;

    private int shaderHandle;
    private int MVPloc;


    private final float[] viewM;
    private final float[] modelM;
    private final float[] inverseModel;
    private final float[] projM;
    private final float[] MVP;

    private int[] VBO;

    private FloatBuffer vertexDataBuffer = null;
    private IntBuffer indexDataBuffer = null;
    private int countFacesToElement;

    private float[] vertexArray;
    private int[] indexArray;

    private int uInverseModel;
    private int uLightPos;
    private int uEyePos;
    private int uCutoff;        /*- uniform location for stadium cutoff -*/
    private int umodelM;

    private static final String vertexShader =  "#version 300 es\n" +
            "\n" +
            "layout(location = 1) in vec3 vPos;\n" +
            "layout(location = 2) in vec3 normal;\n" +
            "uniform mat4 MVP;\n" +
            "uniform mat4 modelMatrix;\n" +
            "uniform mat4 inverseModel;\n" +
            "uniform vec3 lightPos;\n" +
            "uniform vec3 eyePos;\n" +
            "out vec4 finalColor;\n" +
            "out float vertexDepth;\n"+
            "\n" +
            "void main(){\n" +
            "vertexDepth = vPos.z;\n"+
            "vec3 transfNormal = normalize(vec3(inverseModel * vec4(normal,1)));\n" +
            "vec3 fragModel = vec3(modelMatrix * vec4(vPos,1));\n" +
            "vec4 specComponent = vec4(0.92,0.94,0.69,1);\n"+
            "vec4 diffuseComponent = vec4(0.64,0.84,0.15,1);\n"+
            "vec4 ambientComponent = vec4(0.12,0.4,0.01,1);\n" +
            "vec3 eyeDir = normalize(eyePos-fragModel);\n"+
            "vec3 lightDir = normalize(lightPos-fragModel);\n"+
            "float diff = max(dot(lightDir,transfNormal),0.0);\n"+
            "vec3 refl = reflect(-lightDir,transfNormal);\n" +
            "float spec =  pow( max(dot(eyeDir,refl),0.0), 32.0);\n"+
            "finalColor = ambientComponent + diff*diffuseComponent + spec*specComponent; \n"+
            "gl_Position = MVP * vec4(vPos,1);\n" +
            "}";


    private static final String fragmentShader = "#version 300 es\n" +
            "\n" +
            "precision mediump float;\n" +
            "\n" +
            "in float vertexDepth;\n"+
            "uniform float cutoff;\n"+
            "\n"+
            "in vec4 finalColor;\n"+
            "out vec4 fragColor;\n" +
            "\n" +
            "void main() {\n" +
            "if(vertexDepth < cutoff) discard;\n"+
            "fragColor = finalColor;\n"+
            "}";

    public Stadium(Context context){

        this.viewM = new float[16];
        this.modelM = new float[16];
        this.projM = new float[16];
        this.MVP = new float[16];
        this.inverseModel = new float[16];

        this.VBO = new int[2];

        Matrix.setIdentityM(this.inverseModel,0);
        Matrix.setIdentityM(this.viewM, 0);
        Matrix.setIdentityM(this.modelM, 0);
        Matrix.setIdentityM(this.projM, 0);
        Matrix.setIdentityM(this.MVP, 0);

        this.rotationAngle = new AtomicInteger(0);
        this.cutoffLevel = new AtomicInteger(-100);

        /* load and parse ply object */
        InputStream is;
        try {
            is = context.getAssets().open("stadium.ply");
            PlyObject po = new PlyObject(is);
            po.parse();
            this.vertexArray = po.getVertices();
            this.indexArray = po.getIndices();
            this.countFacesToElement = this.indexArray.length;

        }catch(IOException | NumberFormatException e){
            e.printStackTrace();
        }
    }

    /**
     * @brief sets up view and projection matrices
     * */
    public void setupViewMatrices(float aspect, float[] eyePos){
        Matrix.perspectiveM(this.projM, 0, 45f, aspect, 0.1f, 100f);

        Matrix.setLookAtM(this.viewM, 0, eyePos[0], eyePos[1], eyePos[2],
                0, -0.4f, 0,
                0, 1f, 0);
    }

    /**
     * compiles vertex and fragment shader and sends uniform values to the shaders
     * */
    public void compileShader(StadiumRenderer renderer) {
        this.shaderHandle = ShaderCompiler.createProgram(
                vertexShader,
                fragmentShader
        );

        this.MVPloc = glGetUniformLocation(this.shaderHandle, "MVP");
        this.umodelM = glGetUniformLocation(shaderHandle, "modelMatrix");
        this.uCutoff = glGetUniformLocation(shaderHandle, "cutoff");
        this.uInverseModel = glGetUniformLocation(shaderHandle,"inverseModel");
        this.uLightPos = glGetUniformLocation(shaderHandle,"lightPos");
        this.uEyePos = glGetUniformLocation(shaderHandle,"eyePos");

        /*- SENDING THOSE VALUES JUST ONCE -*/
        glUseProgram(shaderHandle);
        glUniform3fv(uLightPos,1,renderer.getLightPos(),0);
        glUniform3fv(uEyePos,1,renderer.getEyePos(),0);
        glUseProgram(0);

    }

    /**
     * @brief matrix multipliication of model matrix, MVP matrix and inverse model matrix
     * */
    public void updateMatrices() {

        float[] localModelM = new float[16];
        float[] localMVP = new float[16];
        float[] localInverseModelM = new float[16];
        float[] tmp = new float[16];

        /*- COMPUTE MODEL MATRIX -*/
        Matrix.setIdentityM(localModelM, 0);
        Matrix.translateM(localModelM, 0, 0, 0, 0);
        Matrix.rotateM(localModelM, 0, this.rotationAngle.get(), 0f, 1f, 0f);

        /*- COMPUTE MVP -*/
        Matrix.multiplyMM(tmp, 0, this.projM, 0, this.viewM, 0);
        Matrix.multiplyMM(localMVP, 0, tmp, 0, localModelM, 0);

        /*- COMPUTE INVERSE MODEL MATRIX -*/
        Matrix.invertM(localInverseModelM, 0,localModelM,0);

        synchronized (this.modelM) {
            System.arraycopy(localModelM, 0, this.modelM, 0, localModelM.length);
        }

        synchronized (this.MVP) {
            System.arraycopy(localMVP, 0, this.MVP, 0, localMVP.length);
        }

        synchronized (this.inverseModel) {
            System.arraycopy(localInverseModelM, 0, this.inverseModel,
                    0, localInverseModelM.length);
        }
    }

    /**
     * @brief gpu side memory allocation and transfer and attributes definition
     * */
    public void setupBuffers(int[] VAO, int index) {

        glGenBuffers(2, VBO, 0);

        GLES30.glBindVertexArray(VAO[index]);

        glBindBuffer(GL_ARRAY_BUFFER, this.VBO[0]);

        glBufferData(GL_ARRAY_BUFFER,
                Float.BYTES * this.getVertexDataFloatBuffer().capacity(),
                this.getVertexDataFloatBuffer(),
                GL_STATIC_DRAW);

        glVertexAttribPointer(1, 3, GL_FLOAT,
                false, 6*Float.BYTES, 0); //vpos
        glVertexAttribPointer(2,3,GL_FLOAT,
                false, 6*Float.BYTES, 3*Float.BYTES); //color/normal
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO[1]);

        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                Integer.BYTES * this.getIndexDataIntBuffer().capacity(),
                this.getIndexDataIntBuffer(),
                GL_STATIC_DRAW);

        GLES30.glBindVertexArray(0);

    }

    /**
     * @brief rotates stadium
     * */
    public void rotateStadium(int angle){
        this.rotationAngle.set(angle);
    }

    /**
     * sets the cutoff level of the stadium in order to delete/restore a portion of the mesh
     * */
    public void cutoffStadium(int cutoff){
        this.cutoffLevel.set(cutoff);
    }

    /**
     * @brief sends uniform values and calls GL drawcall
     * */
    public void drawStadium(int[] VAO, int index){
        glUseProgram(shaderHandle);
            GLES30.glBindVertexArray(VAO[index]);
                glUniform1f(uCutoff, this.cutoffLevel.get() / 70f);

                synchronized (modelM) {
                    glUniformMatrix4fv(umodelM, 1, false, modelM, 0);
                }
                synchronized (this.MVP) {
                    glUniformMatrix4fv(MVPloc, 1, false, MVP, 0);
                }

                synchronized (this.inverseModel) {
                    /* transpose: true -> transpose while sending */
                    glUniformMatrix4fv(uInverseModel, 1, true, inverseModel, 0);
                }

                glDrawElements(GL_TRIANGLES, this.countFacesToElement, GL_UNSIGNED_INT, 0);
            GLES30.glBindVertexArray(0);
        glUseProgram(0);
    }

    /**
     * @brief private method that implements vertex data in a ByteBuffer object
     * */
    private IntBuffer getIndexDataIntBuffer(){

        if(this.indexDataBuffer == null) {
            this.indexDataBuffer =
                    ByteBuffer.allocateDirect(this.indexArray.length * Integer.BYTES)
                            .order(ByteOrder.nativeOrder())
                            .asIntBuffer();
            this.indexDataBuffer.put(this.indexArray);
        }

        this.indexDataBuffer.position(0);
        return this.indexDataBuffer;

    }

    /**
     * @brief private method that implements vertex data in a ByteBuffer object
     * */
    private FloatBuffer getVertexDataFloatBuffer(){

        if(this.vertexDataBuffer == null) {
            this.vertexDataBuffer =
                    ByteBuffer.allocateDirect(this.vertexArray.length * Float.BYTES)
                            .order(ByteOrder.nativeOrder())
                            .asFloatBuffer();
            this.vertexDataBuffer.put(this.vertexArray);
        }

        this.vertexDataBuffer.position(0);
        return this.vertexDataBuffer;
    }

}
