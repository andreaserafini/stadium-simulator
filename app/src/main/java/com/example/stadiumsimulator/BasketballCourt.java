package com.example.stadiumsimulator;

public class BasketballCourt extends Court {

    @Override
    public boolean disposeCourt() {
        boolean[] moved = new boolean[5];

        for (int i = 0; i < 5; i++) {

            moved[i] = false;

            if (super.offsetX.get(i) > (-32 * (4 - i + 1))) {
                super.offsetX.decrementAndGet(i);
                moved[i] = true;
            }

            if (!moved[i]) {
                if (super.offsetY.get(i) > (-32 * (i + 1))) {
                    super.offsetY.decrementAndGet(i);
                    moved[i] = true;
                }
            }

        }

        return moved[0] ||
               moved[1] ||
               moved[2] ||
               moved[3] ||
               moved[4];
    }

    @Override
    public boolean resumeCourt() {
        boolean[] moved = new boolean[5];

        for (int i = 0; i < 5; i++) {

            if (super.offsetY.get(i) < 0) {
                super.offsetY.incrementAndGet(i);
                moved[i] = true;
            }

            if (!moved[i]) {

                if (super.offsetX.get(i) < 0) {
                    super.offsetX.incrementAndGet(i);
                    moved[i] = true;
                }
            }
        }

        return moved[0] ||
               moved[1] ||
               moved[2] ||
               moved[3] ||
               moved[4];
    }
}
