package com.example.stadiumsimulator;

import java.util.TimerTask;

public class MatrixUpdater extends TimerTask {

    private final FootballCourt   footballCourt;
    private final BasketballCourt basketballCourt;
    private final StadiumRenderer stadiumRenderer;
    private final Stadium         stadium;

    private boolean               awake;
    private final Object          lock;

    public MatrixUpdater(StadiumRenderer stadiumRenderer,
                         Stadium stadium,
                         FootballCourt footballCourt,
                         BasketballCourt basketballCourt){

        this.stadiumRenderer = stadiumRenderer;
        this.stadium = stadium;
        this.footballCourt = footballCourt;
        this.basketballCourt = basketballCourt;

        this.awake = true;
        this.lock = new Object();
    }

    @Override
    public void run() {

        if(awake){
            int angle = stadiumRenderer.getAngle().get();

            this.basketballCourt.rotateCourt(angle);
            this.footballCourt.rotateCourt(angle);
            this.stadium.rotateStadium(angle);

            this.stadium.cutoffStadium(stadiumRenderer.getCutoff().get());

            this.basketballCourt.updateMatrices();
            this.footballCourt.updateMatrices();
            this.stadium.updateMatrices();
        }else{
            synchronized(lock){
                try{
                    lock.wait();
                }catch(InterruptedException ignored) {} }
        }

    }

    public void sleep(){
        this.awake = false;
    }

    public void wakeUp(){
        this.awake = true;
        synchronized(this.lock) {
            this.lock.notify();
        }
    }
}

