package com.example.stadiumsimulator;

import static android.opengl.GLES11Ext.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT;
import static android.opengl.GLES11Ext.GL_TEXTURE_MAX_ANISOTROPY_EXT;
import static android.opengl.GLES20.GL_ARRAY_BUFFER;
import static android.opengl.GLES20.GL_CLAMP_TO_EDGE;
import static android.opengl.GLES20.GL_ELEMENT_ARRAY_BUFFER;
import static android.opengl.GLES20.GL_EXTENSIONS;
import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_LINEAR;
import static android.opengl.GLES20.GL_LINEAR_MIPMAP_LINEAR;
import static android.opengl.GLES20.GL_NO_ERROR;
import static android.opengl.GLES20.GL_STATIC_DRAW;
import static android.opengl.GLES20.GL_TEXTURE0;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_MIN_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_S;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_T;
import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.GL_UNSIGNED_INT;
import static android.opengl.GLES20.glActiveTexture;
import static android.opengl.GLES20.glBindBuffer;
import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glBufferData;
import static android.opengl.GLES20.glDrawElements;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGenBuffers;
import static android.opengl.GLES20.glGenTextures;
import static android.opengl.GLES20.glGenerateMipmap;
import static android.opengl.GLES20.glGetError;
import static android.opengl.GLES20.glGetFloatv;
import static android.opengl.GLES20.glGetString;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glTexParameterf;
import static android.opengl.GLES20.glTexParameteri;
import static android.opengl.GLES20.glUniform1i;
import static android.opengl.GLES20.glUniformMatrix4fv;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES30;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

public abstract class Court {

    private float[] vertexArray;
    private int[] indexArray;

    private String vertexShader;
    private String fragmentShader;
    protected int MVPloc;
    private int[] VBO;

    protected int shaderHandle;
    protected int[] texUnit;
    protected int[] texObjId;

    private FloatBuffer vertexDataBuffer = null;
    private IntBuffer indexDataBuffer = null;
    protected int countFacesToElement;

    protected AtomicIntegerArray offsetX;   /** atomic reads/writes */
    protected AtomicIntegerArray offsetY;
    private   AtomicInteger rotationAngle;

    private final float[] viewM;
    private final float[][] modelM;
    private final float[] projM;
    protected final float[][] MVP;

    /* JUST A PASSTHROUGH WITH MVP MULTIPLICATION AND TEXCOORD */
    private static final String defaultVertexShader = "#version 300 es\n" +
            "\n" +
            "layout(location = 1) in vec3 vPos;\n" +
            "layout(location = 2) in vec2 texCoord;\n" +
            "out vec2 varyingTexCoord;\n" +
            "uniform mat4 MVP;\n" +
            "\n" +
            "void main(){\n" +
            "varyingTexCoord = texCoord;\n" +
            "gl_Position = MVP * vec4(vPos,1);\n" +
            "}";

    /* APPLY THE TEXTURE COLOR CORRESPONDING TO THE TEXTURE COORDINATES */
    private static final String defaultFragmentShader = "#version 300 es\n" +
            "\n" +
            "precision mediump float;\n" +
            "\n" +
            "uniform sampler2D courtex;\n" +
            "in vec2 varyingTexCoord;\n" +
            "out vec4 fragColor;\n" +
            "\n" +
            "void main() {\n" +
            "fragColor = texture(courtex,varyingTexCoord);\n" +
            "}";

    /** vertex array */
    private static final float[] defaultVertexArray = new float[]{
            -0.2f, -0.2946f, 0.40f, 0, 1,
            -0.12f, -0.2946f, 0.40f, 0.2f, 1,
            -0.04f, -0.2946f, 0.40f, 0.4f, 1,
            0.04f, -0.2946f, 0.40f, 0.6f, 1,
            0.12f, -0.2946f, 0.40f, 0.8f, 1,
            0.2f, -0.2946f, 0.40f, 1, 1,

            -0.2f, -0.2946f, -0.27f, 0, 0,
            -0.12f, -0.2946f, -0.27f, 0.2f, 0,
            -0.04f, -0.2946f, -0.27f, 0.4f, 0,
            0.04f, -0.2946f, -0.27f, 0.6f, 0,
            0.12f, -0.2946f, -0.27f, 0.8f, 0,
            0.2f, -0.2946f, -0.27f, 1, 0
    };

    /** array of indexes, triangles definition */
    private static final int[] defaultIndexArray = new int[]{
            /* SLICE 0*/
            11, 4, 5,
            11, 10, 4,
            /* SLICE 1 */
            10, 3, 4,
            10, 9, 3,
            /* SLICE 2 */
            9, 2, 3,
            9, 8, 2,
            /* SLICE 3 */
            8, 1, 2,
            8, 7, 1,
            /* SLICE 4 */
            7, 0, 1,
            7, 6, 0
    };

    /**
     * @brief class constructor that uses default values, other constructors can be implemented
     */
    public Court() {

        this.viewM = new float[16];
        this.modelM = new float[5][16]; /*- 4*4 matrix foreach court slice -*/
        this.projM = new float[16];
        this.MVP = new float[5][16]; /*- 4*4 matrix foreach court slice -*/
        this.VBO = new int[2]; //0: vpos, 1: indices
        this.texUnit = new int[1];
        this.texObjId = new int[1];

        this.offsetX = new AtomicIntegerArray(5);
        this.offsetY = new AtomicIntegerArray(5);

        Matrix.setIdentityM(this.viewM, 0);
        Matrix.setIdentityM(this.projM, 0);

        for (int i = 0; i < 5; i++) {
            Matrix.setIdentityM(this.modelM[i], 0);
            Matrix.setIdentityM(this.MVP[i], 0);

            this.offsetX.set(i, 0);
            this.offsetY.set(i, 0);
        }

        this.rotationAngle = new AtomicInteger(0);

        this.vertexArray = defaultVertexArray;
        this.indexArray = defaultIndexArray;
        this.countFacesToElement = defaultIndexArray.length;

        this.vertexShader = defaultVertexShader;
        this.fragmentShader = defaultFragmentShader;
    }


    /**
     * @brief compiles the shader program and returns an handle
     * */
    public void compileShader() {
        /*
        * Didn't use a static shaderHandle because it would have made necessary to add multiple
        * textures in the shader, and therefore the code would have been less reusable.
        * */
        this.shaderHandle = ShaderCompiler.createProgram(
                this.vertexShader,
                this.fragmentShader
        );

        this.MVPloc = glGetUniformLocation(this.shaderHandle, "MVP");
    }

    /**
     * @brief memory allocation and data transfer to GPU memory
     * */
    public void setupBuffers(int[] VAO, int index) {

        glGenBuffers(2, this.VBO, 0);

        GLES30.glBindVertexArray(VAO[index]);

        glBindBuffer(GL_ARRAY_BUFFER, this.VBO[0]);

        glBufferData(
                GL_ARRAY_BUFFER,
                Float.BYTES * this.getVertexDataFloatBuffer().capacity(),
                this.getVertexDataFloatBuffer(),
                GL_STATIC_DRAW
        );

        glVertexAttribPointer(1, 3, GL_FLOAT,
                false, Float.BYTES * 5, 0); //vpos
        glVertexAttribPointer(2, 2, GL_FLOAT,
                false, Float.BYTES * 5, 3 * Float.BYTES); //texcoord
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this.VBO[1]);

        glBufferData(
                GL_ELEMENT_ARRAY_BUFFER,
                Integer.BYTES * this.getIndexDataIntBuffer().capacity(),
                this.getIndexDataIntBuffer(),
                GL_STATIC_DRAW
        );

        GLES30.glBindVertexArray(0);
    }

    /**
     * @brief load court texture and define gl texture parameters
     *
     * @param context context of the application
     * */
    public int loadTexture(Context context, int textureIndex, int resourceIndex) {
        {

            if (this.shaderHandle == -1) return -1;
            this.texUnit[0] = glGetUniformLocation(this.shaderHandle, "courtex");

            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inScaled = false;

            Bitmap bitmap = BitmapFactory.decodeResource(
                    context.getResources(),
                    resourceIndex,
                    opts
            );

            if (bitmap == null)
                return -1;

            this.texObjId = new int[1];
            glGenTextures(1, this.texObjId, 0);

            glBindTexture(GL_TEXTURE_2D, this.texObjId[0]);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            /*- Anisotropic filter activation -*/
            if(glGetString(GL_EXTENSIONS).contains("anisotropic")){
                float[] maxAF = new float[1];
                glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, maxAF, 0);
                Log.v("LOAD TEXTURE", "Max anisotropic filtering is " + maxAF[0]);

                int error = glGetError();
                if(error != GL_NO_ERROR)
                    Log. v("LOAD TEXTURE","Error while retrieving mx AF level " + error);

                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAF[0]);

                error = glGetError();
                if(error != GL_NO_ERROR)
                    Log. v("LOAD TEXTURE","Error while activating AF filter " + error);
                else
                    Log.v("LOAD TEXTURE", "Anisotropic filter activated");
            }else{
                Log.v("LOAD TEXTURE", "Anisotropic filtering is not supported");
            }
            /*- END anisotropic filter -*/

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

            GLUtils.texImage2D(GL_TEXTURE_2D, 0, bitmap, 0);
            glGenerateMipmap(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, 0);

            glActiveTexture(GL_TEXTURE0 + textureIndex);
            glBindTexture(GL_TEXTURE_2D, this.texObjId[0]);

            glUseProgram(this.shaderHandle);
            glUniform1i(this.texUnit[0], textureIndex); /* 1 -> GL_TEXTURE1 */
            glUseProgram(0);
            glBindTexture(GL_TEXTURE_2D, 0);

            bitmap.recycle();

            return 0;
        }
    }

    /**
     * @brief sets up View and Projection matrixes
     * */
    public void setupViewMatrices(float aspect, float[] eyePos) {

        Matrix.perspectiveM(this.projM, 0, 45f, aspect, 0.1f, 100f);

        Matrix.setLookAtM(this.viewM, 0, eyePos[0], eyePos[1], eyePos[2],
                0, -0.4f, 0,
                0, 1f, 0);
    }

    /**
     * @brief traslate model and rotate it
     */
    public void updateMatrices() {

        float[] tmp = new float[16];
        Matrix.multiplyMM(tmp, 0, this.projM, 0, this.viewM, 0);

        for (int i = 0; i < 5; i++) {

            float currOffX = this.offsetX.get(i) / 400f;
            float currOffY = this.offsetY.get(i) / 400f;

            /*- ModelM IS NOT LOCKED BECAUSE IT'S NOT USED IN THE OTHER THREAD -*/
            Matrix.setIdentityM(this.modelM[i], 0);
            Matrix.rotateM(this.modelM[i], 0, this.rotationAngle.get(), 0f, 1f, 0f);
            Matrix.translateM(this.modelM[i], 0, currOffX, currOffY, 0);


            /* FINE GRAIN LOCKING HERE */
            synchronized (this.MVP[i]) {
                Matrix.multiplyMM(this.MVP[i], 0, tmp, 0, this.modelM[i], 0);
            }
        }
    }

    /**
     * @brief update the rotation angle of the model
     *
     * @param angle new rotation angle
     * */
    public void rotateCourt(int angle){
        this.rotationAngle.set(angle);
    }

    /**
     * @brief increment X offset and Y offset to dispose the court
     *
     * @return true if still moving, false if finished disposing
     * */
    public abstract boolean disposeCourt();

    /**
     * @brief decrement X and Y offset do resume the disposed court
     *
     * @return true if still moving, false if finished resuming
     * */
    public abstract boolean resumeCourt();

    /**
     * @brief disposes a court by calling @disposeCourt()
     * */
    public void setDisposed(){
        while(disposeCourt());
    }

    /**
     * @brief contains openGl draw call
     * */
    public void drawCourt(int[] VAO, int index, int textureIndex) {
        glActiveTexture(GL_TEXTURE0 + textureIndex);
        glBindTexture(GL_TEXTURE_2D, this.texObjId[0]);

        glUseProgram(this.shaderHandle);
        GLES30.glBindVertexArray(VAO[index]);
        /*- ONE TRANSFER AND ONE DRAWCALL FOREACH COURT SLICE -*/
        for (int i = 0; i < 5; i ++){
            synchronized (this.MVP[i]) {
                glUniformMatrix4fv(this.MVPloc, 1, false, this.MVP[i], 0);
            }
            glDrawElements(GL_TRIANGLES,
                    this.countFacesToElement / 5,
                    GL_UNSIGNED_INT,
                    i * this.countFacesToElement / 5 * Integer.BYTES);
        }
        GLES30.glBindVertexArray(0);
        glUseProgram(0);
        glBindTexture(GL_TEXTURE_2D,0);
    }

    /**
     * @brief private method that allocates index data in a ByteBuffer object
     * */
    private IntBuffer getIndexDataIntBuffer(){

        if(this.indexDataBuffer == null) {
            this.indexDataBuffer =
                    ByteBuffer.allocateDirect(this.indexArray.length * Integer.BYTES)
                            .order(ByteOrder.nativeOrder())
                            .asIntBuffer();
            this.indexDataBuffer.put(this.indexArray);
        }

        this.indexDataBuffer.position(0);
        return this.indexDataBuffer;

    }

    /**
     * @brief private method that implements vertex data in a ByteBuffer object
     * */
    private FloatBuffer getVertexDataFloatBuffer(){

        if(this.vertexDataBuffer == null) {
            this.vertexDataBuffer =
                    ByteBuffer.allocateDirect(this.vertexArray.length * Float.BYTES)
                            .order(ByteOrder.nativeOrder())
                            .asFloatBuffer();
            this.vertexDataBuffer.put(this.vertexArray);
        }

        this.vertexDataBuffer.position(0);
        return this.vertexDataBuffer;
    }



}
